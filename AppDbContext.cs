﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connect = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=SplitQuery;Trusted_Connection=True;TrustServerCertificate=True";
        optionsBuilder.UseSqlServer(connect);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
                    .Property(x => x.Name)
                    .HasMaxLength(100);

        modelBuilder.Entity<Post>()
                    .Property(x => x.Title)
                    .HasMaxLength(100);

        modelBuilder.Entity<Post>()
                    .HasOne(x => x.User)
                    .WithMany(x => x.Posts)
                    .HasForeignKey(x => x.UserId);

        modelBuilder.Entity<User>()
                    .HasData(
                        new User { Id = 1, Name = "User A" },
                        new User { Id = 2, Name = "User B" }
                    );

        modelBuilder.Entity<Post>()
                    .HasData(
                        new Post { Id = 1, UserId = 1, Title = "Post A" },
                        new Post { Id = 2, UserId = 1, Title = "Post B" }
                     );

    }
}

public class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<Post> Posts { get; set; }
}

public class Post
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public User User { get; set; }
    public string Title { get; set; }
}