﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;

Console.WriteLine("Hello, World!");
AppDbContext dbcontext = new AppDbContext();

// eager loading
//List<User> userWithPost = dbcontext.Users
//                            .Include(x => x.Posts)
//                            .ToList();



// Define a LINQ query that selects users with their posts
var query = dbcontext.Users.Include(u => u.Posts);

// Split the query into two separate queries
List<User> users = query.AsSplitQuery().ToList(); // retrieves all users with their posts

Console.WriteLine();
